from setuptools import setup
import setuptools

setup(
    name='calculatorjulien_package_julienfal',
    version='0.0.1',
    author="Julien Falgayrettes",
    description="calculatorjulien is a simple package \
    in order to make some test on packaging principles in Python",
    license='GNU GPLv3',
    package_dir={"": "."},
    packages=['calculator'],
)
