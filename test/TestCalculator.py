import logging
from calculator.SimpleCalculator import SimpleCalculator
import unittest
import logging

class TestCalcul(unittest.TestCase):

    def setUp(self):
        self.calculator=SimpleCalculator(8,2)
        self.calculatorbis=SimpleCalculator(8,0)

    def test_sum(self):
        result = self.calculator.f_sum()
        self.assertEqual(result,10)
        logging.warning('test_sum ok warn')
        logging.info("test_sum ok info")
    
    def test_sum_integers(self):
        result = self.calculator.f_sum()
        self.assertEqual(result,"ERROR")

    def test_substract(self):
        result = self.calculator.f_substract()
        self.assertEqual(result,6)
        logging.warning('test_substract ok warn')
        logging.info("test_substract ok info")

    def test_divide(self):
        with self.assertRaises(ZeroDivisionError):
            self.calculatorbis.f_divide()
            logging.warning('test_divide ok warn')
            logging.info("test_divide ok info")

    def test_nbrentier(self):
        self.assertIsInstance(self.calculator.nb1,int)
    


if __name__ == '__main__':
    unittest.main()
